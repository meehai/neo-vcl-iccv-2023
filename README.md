# Multi Layer Hypergraphs for Semi-supervised learning using Earth Observations

<img src="figure.png" width="70%">

Paper accepted at [Visual continual learning ICCV 23 Workshop](https://wvcl.vis.xyz/)

Poster: [link](https://drive.google.com/file/d/18XmoEeL0ZNuPv4nFvch-fGzdQlrwKBcG/)

Install the requirements: `pip install -r data/requirements.txt`

## 1. Dataset

Dataset is also hosted at: [link](https://huggingface.co/datasets/Meehai/neo_data_1month)

### 1.1 Download the PNG dataset

```
python data/download_raw_data.py 1month --download -o data/neo_raw_data
```

### 1.2 Convert PNG to NPY dataset

```
python data/convert_png_to_npy.py data/neo_raw_data -o data/neo_data
```

### 1.3 Filter only relevant data

```
python data/split_train_test_semisup.py --dataset_path data/neo_data \
    --in_nodes CHLORA CLD_FR CLD_RD CLD_WP COT LSTD LSTN NO2 NDVI OZONE SNOWC SST \
    --out_nodes AOD CarbonMonoxide FIRE LSTD_AN LSTN_AN WV LAI
```

You now have 3 directories: `data/train_set`, `data/test_set` and `data/semisupervised_set`.

## 2. Train 1st iteration (supervised) and dump initial pseudo-labels for nn/lr aggregation

### 2.1 Train the 1st iteration graph

```
CUDA_VISIBLE_DEVICES=... ./train/train.py \
    --config_path train/cfgs/iccv23/graph_ensemble_option_a.yml \
    --dataset_path data/train_set/ \
    --validation_set_path data/test_set/ \
    [--parallelize]
```

Note: Use `--parallelize` if you have more gpus, passed by `CUDA_VISIBLE_DEVICES=0,1,..` variable.


### 2.2 Dump predictions for the train set and test set using the iter 1 graph

These are used to train the aggregation NN and LR.

```
CUDA_VISIBLE_DEVICES=1 python train/dump_predictions.py \
    --dataset_path data/train_set \
    --config_path train/cfgs/iccv23/graph_ensemble_option_a.yml \
    -o dump_dirs/dump_dir_neo_iter1_train \
    --weights_path train/experiments/graph_ensemble_option_a/iter1/models/ \
    [--dump_png] # this also exports PNGs
```

```
CUDA_VISIBLE_DEVICES=1 python train/dump_predictions.py \
    --dataset_path data/test_set \
    --config_path train/cfgs/iccv23/graph_ensemble_option_a.yml \
    -o dump_dirs/dump_dir_neo_iter1_test \
    --weights_path train/experiments/graph_ensemble_option_a/iter1/models/ \
    [--dump_png] # this also exports PNGs
```

### 2.3 Do selection for all 3 aggregations (mean, lr, nn)

```
python train/selection_and_ensemble.py \
    dump_dirs/dump_dir_neo_iter1_train \
    dump_dirs/dump_dir_neo_iter1_test \
    --variants simple_mean weighted_mean_lr nn_small_v2
```

This should give you in `dump_dirs/dump_dir_neo_iter1_train` 2 set of csvs:
- per node, per edge results of the 1st supervised iteration. Example: `AOD_results_edges_test.csv`.
- per node, per variant results after selection. Example: `AOD_results_simple_mean.csv`, `AOD_results_nn_small_v2.csv` etc.

Note: `simple_mean` and `nn_small_v2` (and v1, v3, but not presented here) will train and store artifacts.

## 3. Train 2nd iteration

### 3.1 Train iter2 using simple_mean ensemble aggregation


#### Update the CFGs based on the selection in the csvs above.
For this, you need to go in `train/cfgs/iccv23/graph_ensemble_option_a.yml` and make a new file `train/cfgs/iccv23/graph_ensemble_option_a_iter2_mean.yml` where only the edges described in the csvs above are kept. top-5 means the first 5
edges based on the `node_results_edges_train.csv` file. Use the `train` results, not `test` results, to not leak results!

```
CUDA_VISIBLE_DEVICES=... ./train/train.py \
    --config_path train/cfgs/iccv23/graph_ensemble_option_a_iter2_mean.yml \
    --dataset_path data/train_set/ \
    --validation_set_path data/test_set/ \
    --semisupervised_set_path data/semisupervised_set \
    [--parallelize]
```

### 3.2 Train iter2 using linear regression ensemble aggregation (weighted_mean_lr) & network ensemble aggregation (nn_small_v2)

- see a cfg example in `cfgs/graph_ensemble_option_a_nn.yml` and `vote_fn_nn.py`. You need to update some hardcoded
paths to account for your own paths.

```
CUDA_VISIBLE_DEVICES=... ./train/train.py \
    --config_path train/cfgs/iccv23/graph_ensemble_option_a_iter2_nn.yml \ # must be updated by you
    --dataset_path data/train_set/ \
    --validation_set_path data/test_set/ \
    --semisupervised_set_path data/semisupervised_set \
    [--parallelize]
```

### 4. Evaluate

After 3.1 and 3.2 was done properly for 1 or 2 more iterations, we evaluate by doing the same dump dir script.
Update iterX with your iteration and the experiment dir based on which iteration ensemble (simple_mean, lr, nn-v2)
was used.

#### 4.1 Dump dir
```
CUDA_VISIBLE_DEVICES=1 python train/dump_predictions.py \
    --dataset_path data/test_set \
    --config_path train/cfgs/iccv23/graph_ensemble_option_a_iterX.yml \
    -o dump_dirs/dump_dir_neo_iterX_test \
    --weights_path train/experiments/graph_ensemble_option_a/iterX/models/ \
    [--dump_png] # this also exports PNGs
```

#### 4.2 Run evaluation script

```
python train/evaluate_dumped_predictions.py dump_dirs/dump_dir_neo_iterX_test
```

You should now have CSV results for this iteration.

