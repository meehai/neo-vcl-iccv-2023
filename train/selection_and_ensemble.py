#!/usr/bin/env python3
from argparse import ArgumentParser
from pathlib import Path
from natsort import natsorted
import numpy as np
import pandas as pd
from typing import Dict, Callable, Tuple, List
from numpy.typing import NDArray
from tqdm import tqdm, trange
import os
from simple_caching import cache_fn
from simple_caching.storage import NpyFS
from sklearn.linear_model import LinearRegression

from nn_small import nn_small_v1, nn_small_v2, nn_small_v3

EXPECTED_N_COUNT = (
    30,
    92,
)  # nasa dataset has 30 test set items. test + train-right has 92.

# TODO -- these are passed further to nns
g_variant = None
node_name = None
res_edges = None

# generic required functions


def do_metric(y_data: NDArray["n,h,w,c"], gt_data: NDArray["n,h,w,c"]) -> NDArray["n"]:
    """Appies the metric_fn for the (y, gt) data. Returns the per-item metric score."""
    assert len(y_data) == len(gt_data)
    res = []
    # Apply the metric_for each aggregated item item individually
    for y, gt in zip(y_data, gt_data):
        res.append(l2(y, gt))
    res = np.array(res, dtype=np.float32)
    res[np.isnan(res)] = res[~np.isnan(res)].mean()
    assert res.shape == (len(y_data),), res.shape
    return res


def filter_list(y_data, key_list):
    keys = [key for key in y_data.keys() if key in key_list]
    filtered_data = {k: v for k, v in y_data.items() if k in keys}
    filtered_data = {k: filtered_data[k] for k in key_list}
    return filtered_data


def name_from_fn(fn: Callable) -> str:
    return str(fn).split(" ")[1]


def f_selection(
    variant_name: str,
    variant_fn: Callable,
    y_train: Dict[str, np.ndarray],
    gt_train: np.ndarray,
    y_test: Dict[str, np.ndarray],
    gt_test: np.ndarray,
    res_edges: pd.Series,
) -> pd.Series:
    """Does a for loop based on the res_edges order and applies the variant on each top-2 -> top-N voters"""
    res = {}
    global top_res_edges, g_variant
    g_variant = variant_name
    for i in trange(2, len(res_edges) + 1, desc=f"top selection {variant_name}"):
        top_i_index = res_edges[0:i].index
        top_y_train = np.stack(tuple(filter_list(y_train, top_i_index).values()))
        top_y_test = np.stack(tuple(filter_list(y_test, top_i_index).values()))

        top_res_edges = res_edges.iloc[0:i]
        values = variant_fn(top_y_train, gt_train, top_y_test, gt_test)
        # We may return the aggregated items + some extra stuff (like linear regression weights)
        values, others = (
            (values[0], values[1]) if isinstance(values, tuple) else (values, {})
        )
        assert len(values.shape) == 4 and values.shape[0] in EXPECTED_N_COUNT, values.shape
        out = do_metric(values, gt_test)
        key = f"{variant_name}-top_{i}"
        res[key] = {
            "l2": out.mean(),
            "l2_std": out.std(),
            "l2_each": tuple(round(x, 5) for x in out),
            **others,
        }

    res = pd.DataFrame(res).T.sort_values("l2")
    return res


# Read data and such


def read_all(paths: Dict[str, List[Path]]):
    res = {}
    for k, edge_paths in tqdm(paths.items(), desc="Read data"):
        edge_data = np.array([np.load(y)["arr_0"] for y in edge_paths], dtype=np.float32)
        edge_data[np.isnan(edge_data)] = 0
        res[k] = edge_data
    return res


def _natsort_iterdir(dir: Path) -> List[Path]:
    """iterdirs() a directory with expected predictions and returns the list of all files, natsorted"""
    return natsorted([x for x in dir.iterdir()], key=lambda x: x.name)


def read_node_data_cache_fn(node_path: Path) -> str:
    # Returns parent/node_name, so we can work with multiple iterations too, stored in different dump_dirs
    return f"{node_path.parent.name}_{node_path.name}"


# @cache_fn(NpyFS, key_encode_fn=read_node_data_cache_fn)
def read_node_data(node_path: Path) -> Tuple[Dict[str, np.ndarray], np.ndarray]:
    """Traverseses the dump_dir structure of a node, reads edges predictions and gt and returns them."""
    gt_path = node_path / "gt/state/npy"
    gt_data_paths = _natsort_iterdir(gt_path)
    gt_data: np.ndarray = read_all({"GT": gt_data_paths})["GT"]

    all_timestamps = [x for x in node_path.iterdir() if x.name != "gt"]
    all_edges = {}
    for timestamp_dir in all_timestamps:
        t_edges = [
            edge_dir / "npy"
            for edge_dir in timestamp_dir.iterdir()
            if edge_dir.name != "state"
        ]
        for t_edge in t_edges:
            if t_edge.parent.name in all_edges:
                print(
                    f"t={timestamp_dir}. Edge '{t_edge.parent.name}'. Already read before, will overwrite."
                )
            all_edges[t_edge.parent.name] = _natsort_iterdir(t_edge)
    assert len(all_edges) > 0, f"'{node_path}' has 0 usable edges"
    y_data: Dict[str, np.ndarray] = read_all(all_edges)
    for k, v in y_data.items():
        assert v.shape == gt_data.shape, f"edge {k}. {v.shape} vs {gt_data.shape}"
    return y_data, gt_data


def get_output_nodes(base_dir: Path):
    """Infering output nodes from base dir based on whethere there's only GT or there's predictions, too"""
    dump_dirs = tuple(x for x in Path(base_dir).iterdir() if x.is_dir())
    out_nodes = []
    for dump_dir in dump_dirs:
        if len(list(dump_dir.iterdir())) == 1:
            continue
        if dump_dir.name == "logs":
            continue
        if not (dump_dir / "gt/state/npy").exists():
            print(f"Potential out node: '{dump_dir.name}' has no GT data. Skipping.")
            continue
        out_nodes.append(dump_dir.name)
    return sorted(out_nodes)


# Metrics stuff below


def do_edge_metrics(y_data, gt_data) -> pd.Series:
    res = {}
    for key in tqdm(y_data.keys(), desc="all metrics"):
        ys = y_data[key]
        out = do_metric(ys, gt_data)
        res[key] = {
            "l2": out.mean(),
            "l2_std": out.std(),
            "l2_each": tuple(round(x, 5) for x in out),
        }
    res = pd.DataFrame(res).T.sort_values("l2")
    res.index.name = "edges"
    res["l2 relative"] = rel(res["l2"].min(), res["l2"])
    return res


def get_weights_v1(y_data: np.ndarray, res: pd.Series):
    assert len(res) == len(y_data)
    A: pd.Series = (res.max() - res) + res.min()
    A = A / A.sum()
    return A.values


def get_weights_lr_key_encode_fn(
    y_data: np.ndarray, gt_data: np.ndarray, positive: bool
):
    part1 = f"{y_data.mean():.5f}_{y_data.std():.5f}"
    part2 = f"{gt_data.mean():5f}_{gt_data.std():.5f}"
    part3 = int(positive)
    return f"{part1}_{part2}_{part3}"


@cache_fn(NpyFS, key_encode_fn=get_weights_lr_key_encode_fn)
def get_weights_lr(y_data: np.ndarray, gt_data: np.ndarray, positive: bool):
    y_data = np.array(y_data).reshape(len(y_data), -1).T
    gt_data = np.array(gt_data).flatten()

    y_data = y_data[gt_data != 0]
    gt_data = gt_data[gt_data != 0]
    lr = LinearRegression(positive=positive)
    lr.fit(y_data, gt_data)
    weights = lr.coef_
    return weights / weights.sum()


# simple aggregation functions working on 1 item only (not a batch)


def agg_median(y_data: NDArray["v,h,w,c"]) -> NDArray["h,w,c"]:
    assert len(y_data.shape) == 4 and y_data.shape[-1] == 1
    return np.median(y_data, axis=0)


def agg_mean(y_data: NDArray["v,h,w,c"]) -> NDArray["h,w,c"]:
    assert len(y_data.shape) == 4 and y_data.shape[-1] == 1
    return np.mean(y_data, axis=0)


def agg_weighted_mean(
    y_data: NDArray["v,h,w,c"], weights: NDArray["v"]
) -> NDArray["h,w,c"]:
    assert len(y_data.shape) == 4 and y_data.shape[-1] == 1
    assert np.fabs(weights.sum() - 1) < 1e-5, f"Got {weights.sum()}"
    return np.einsum("vijk,v->ijk", y_data, weights)


# simple metrics
def l2(y: NDArray["h,w,c"], gt: NDArray["h,w,c"]) -> float:
    assert len(y.shape) == 3 and len(gt.shape) == 3
    y = y.flatten().astype(float)
    gt = gt.flatten().astype(float)
    mask = gt != 0
    y = y[mask]
    gt = gt[mask]
    return ((y - gt) ** 2).mean()


def rel(baseline: NDArray["n"], x: NDArray["n"]) -> NDArray["n"]:
    return ((baseline / x - 1) * 100).astype(float)


# Variants of aggregation functions. THESE FUNCTIONS ONLY AGGREGATE v,n,h,w,c -> n,h,w,c


def simple_mean(
    y_train: NDArray["v,n,h,w,c"],
    gt_train: NDArray["n,h,w,c"],
    y_test: NDArray["v,n,h,w,c"],
    gt_test: NDArray["n,h,w,c"],
) -> NDArray["n,h,w,c"]:
    return y_test.mean(axis=0)


def simple_median(
    y_train: NDArray["v,n,h,w,c"],
    gt_train: NDArray["n,h,w,c"],
    y_test: NDArray["v,n,h,w,c"],
    gt_test: NDArray["n,h,w,c"],
) -> NDArray["n,h,w,c"]:
    return np.median(y_test, axis=0)


def wmean_lr(
    y_train: NDArray["v,n,h,w,c"],
    gt_train: NDArray["n,h,w,c"],
    y_test: NDArray["v,n,h,w,c"],
    gt_test: NDArray["n,h,w,c"],
) -> Tuple[NDArray["n,h,w,c"], Dict[str, np.ndarray]]:
    weights = get_weights_lr(y_train, gt_train, positive=False)
    res = np.array([agg_weighted_mean(_y, weights) for _y in y_test.transpose(1, 0, 2, 3, 4)])
    res_weights = tuple(round(x, 3) for x in weights)
    return res, {"weights": res_weights}


def compute_nn_small_v1(
    y_train: NDArray["v,n,h,w,c"],
    gt_train: NDArray["n,h,w,c"],
    y_test: NDArray["v,n,h,w,c"],
    gt_test: NDArray["n,h,w,c"],
) -> Dict[str, float]:
    global node_name, top_res_edges, g_variant
    y = nn_small_v1(
        y_train, gt_train, y_test, gt_test, node_name, g_variant, top_res_edges
    )
    return y


def compute_nn_small_v2(
    y_train: NDArray["v,n,h,w,c"],
    gt_train: NDArray["n,h,w,c"],
    y_test: NDArray["v,n,h,w,c"],
    gt_test: NDArray["n,h,w,c"],
) -> Dict[str, float]:
    global node_name, top_res_edges, g_variant
    y = nn_small_v2(
        y_train, gt_train, y_test, gt_test, node_name, g_variant, top_res_edges
    )
    return y


def compute_nn_small_v3(
    y_train: NDArray["v,n,h,w,c"],
    gt_train: NDArray["n,h,w,c"],
    y_test: NDArray["v,n,h,w,c"],
    gt_test: NDArray["n,h,w,c"],
) -> Dict[str, float]:
    global node_name, top_res_edges, g_variant
    y = nn_small_v3(y_train, gt_train, y_test, gt_test, node_name, g_variant, top_res_edges)
    return y


def get_args():
    parser = ArgumentParser()
    parser.add_argument("train_dir", type=lambda p: Path(p).absolute())
    parser.add_argument("test_dir", type=lambda p: Path(p).absolute())
    parser.add_argument("--variants", nargs="+")
    parser.add_argument("--nodes", nargs="+")
    args = parser.parse_args()
    args.variants = [] if args.variants is None else args.variants
    assert Path(args.train_dir).exists(), args.train_dir
    assert Path(args.test_dir).exists(), args.test_dir
    if args.nodes is None:
        args.nodes = get_output_nodes(Path(args.train_dir))
        print(f"--nodes not provided. Reading all nodes from train dir: '{args.train_dir}': {args.nodes}")
    return args


def main():
    args = get_args()
    os.chdir(args.train_dir)

    print(f"Train dir: '{args.train_dir}'. \nTest dir: '{args.test_dir}'. \nOut nodes: {args.nodes}")
    global node_name

    # aggregation functions
    possible_variants = {
        "simple_mean": simple_mean,
        "simple_median": simple_median,
        "weighted_mean_lr": wmean_lr,
        "nn_small_v1": compute_nn_small_v1,
        "nn_small_v2": compute_nn_small_v2,
        "nn_small_v3": compute_nn_small_v3,
    }
    diff = list(set(args.variants).difference(possible_variants.keys()))
    assert len(diff) == 0, f"Variants that are not implemented: {diff}. Possible: {list(possible_variants.keys())}"
    print(f"Chosen variants: {args.variants}")

    pbar = tqdm(args.nodes)
    for node in pbar:
        pbar.set_description(node)
        node_name = node
        y_train, gt_train = read_node_data(args.train_dir / node)
        y_test, gt_test = read_node_data(args.test_dir / node)
        print(f"Node: '{node}'. Train set: {gt_train.shape}. Test set: {gt_test.shape}. N voters: {len(y_train)}")
        assert len(y_train) == len(y_test), f"{len(y_train)} vs {len(y_test)}"
        assert sorted(list(y_train.keys())) == sorted(list(y_test.keys())), (list(y_train.keys()), list(y_test.keys()))

        # Get the results of all edges first on test set
        res_edges_out_file = Path(f"{args.train_dir}/{node}_results_edges_test.csv")
        print(f"{res_edges_out_file}. Exists: {res_edges_out_file.exists()}")
        if not res_edges_out_file.exists():
            res_edges = do_edge_metrics(y_test, gt_test)
            res_edges.to_csv(res_edges_out_file, float_format="%.5f")
        else:
            res_edges = pd.read_csv(res_edges_out_file, index_col=0)

        # Get the results of all edges first on train set as well (not used for relative down below, only test results)
        res_edges_train_out_file = Path(f"{args.train_dir}/{node}_results_edges_train.csv")
        print(f"{res_edges_train_out_file}. Exists: {res_edges_train_out_file.exists()}")
        if not res_edges_train_out_file.exists():
            res_edges_train = do_edge_metrics(y_train, gt_train)
            res_edges_train.to_csv(res_edges_train_out_file, float_format="%.5f")

        for variant in args.variants:
            variant_fn = possible_variants[variant]
            # dump_dir/AOD_results_simple_mean.csv
            out_file = Path(f"{args.train_dir}/{node}_results_{variant}.csv")
            print(f"{out_file}. Exists: {out_file.exists()}")
            if not out_file.exists():
                res = f_selection(variant, variant_fn, y_train, gt_train, y_test, gt_test, res_edges["l2"],)
                res.to_csv(out_file, float_format="%.5f")
            else:
                res = pd.read_csv(out_file, index_col=0)


if __name__ == "__main__":
    main()
