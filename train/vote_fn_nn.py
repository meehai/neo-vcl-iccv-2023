from functools import partial
from typing import Dict, List, Callable, Any
import torch as tr
import importlib
from pathlib import Path

from lightning_module_enhanced import LME

def module_from_file(module_name: str, file_path: Path) -> Any:
    """
    Returns a class or function by name from an arbitrary file given its path.
    Usage:
    - X_type = module_from_file("ClassName", "/path/to/module.py")
    - X = X_type(params)
    """
    spec = importlib.util.spec_from_file_location(module_name, file_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return getattr(module, module_name)

def _agg_nn(votes: "tr.Tensor[bvwhd]", sources: List[str], nns: Dict[str, LME]) -> "tr.Tensor[bwhd]":
    """Vote function based on a pre-trained neural network, given as a dict for each noe"""
    # Get the node name
    node = sources[0].split(" -> ")[-1]
    # Keep only the relevant sources based on n_votes inside the nn module
    relevant_sources = nns[node].hparams["edges"]
    # Find the indexes of these votes where the top n_votes reside
    ixs = [sources.index(rel) for rel in relevant_sources]
    # Get only these votes (input: bvwhd)
    relevant_votes = votes[:, ixs]
    res = nns[node].forward(relevant_votes)
    return res

def vote_fn_nn(iteration: int) -> Callable:
    assert iteration <= 2, iteration
    if iteration == 1:
        logs_dir = "/scratch/nvme0n1/ngc/ngc-analyze-dump-dir/dump_dirs/nasa/dump_dir_it1_train/logs"
        tops = {
            "AOD": 14,
            "CarbonMonoxide": 11,
            "FIRE": 16,
            "LAI": 9,
            "LSTD_AN": 7,
            "LSTN_AN": 10,
            "WV": 6,
        }

    if iteration == 2:
        logs_dir = "/scratch/nvme0n1/ngc/ngc-analyze-dump-dir/dump_dirs/nasa/dump_dir_it2_nn_train/logs"
        # tops can be extracted from {node}_top_only.csv or {node}_results_nn_small_v3.csv in future.
        tops = {
            "AOD": 2,
            "CarbonMonoxide": 10,
            "FIRE": 5,
            "LAI": 4,
            "LSTD_AN": 2,
            "LSTN_AN": 2,
            "WV": 5,
        }

    variant = "nn_small_v2"
    model_type = {
        "nn_small_v2": "ModelConv3D_V2"
    }[variant]
    paths = {node: f"{logs_dir}/{node}/{variant}/top_{tops[node]}/checkpoints/model_best.ckpt" for node in tops.keys()}
    nn = module_from_file(model_type, "/scratch/nvme0n1/ngc/ngc-analyze-dump-dir/nn_small.py")
    nns = {node: LME(nn(tops[node], [10, 5])).load_state_from_path(v) for node, v in paths.items()}
    return partial(_agg_nn, nns=nns)
