#!/usr/bin/env python3
"""Dump predictions at edge-level for your NGC."""
from functools import partial
from pathlib import Path
from argparse import ArgumentParser
from omegaconf import OmegaConf
import torch as tr

from torch.utils.data import DataLoader
from lightning_module_enhanced import LME
from ngclib.logger import logger
from ngclib.readers import NGCNpzReader
from ngclib.graph_cfg import GraphCfg, NGCNodesImporter
from ngclib.utils import dump_predictions
from ngclib.trainer import TrainableNGC

from aggregation import aggregation_fn_from_cfg
from models import get_model_type

device = tr.device("cuda" if tr.cuda.is_available() else "cpu")

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--dataset_path", type=lambda p: Path(p).absolute(), required=True)
    parser.add_argument("--config_path", type=Path, required=True, help="Path to the data & graph config file")
    parser.add_argument("--weights_path", type=Path, required=True, help="Path to models dir where weights are. For "
                        "async-trained NGC, this is the path to models/ dir. For sync-trained NGC, path to .ckpt")
    parser.add_argument("--nodes_path", type=Path, required=True, help="Path to the nodes definition")
    parser.add_argument("--output_path", "-o", type=lambda p: Path(p).absolute(), required=True)
    parser.add_argument("--dump_png", action="store_true")
    parser.add_argument("--overwrite", action="store_true")
    args = parser.parse_args()
    assert not args.output_path.exists() or args.overwrite, f"Path '{args.output_path}' exists. Use --overwrite."
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    graph_cfg = GraphCfg(OmegaConf.to_container(cfg.graph))
    nodes = NGCNodesImporter.from_graph_cfg(graph_cfg, nodes_module_path=args.nodes_path).nodes
    logger.info(f"Dump predictions."
                f"\n- Dataset path: '{args.dataset_path}."
                f"\n- Config path: '{args.config_path}'"
                f"\n- Nodes: {graph_cfg.node_names}"
                f"\n- Weights dir: '{args.weights_path}'."
                f"\n- Out path: '{args.output_path}'")

    out_nodes = [node.name for node in nodes if node.name in graph_cfg.output_nodes]
    # only test-mode in this. For semi-supervised, use ngc-analyze-dir repo dump predictions script.
    reader = NGCNpzReader(path=args.dataset_path, nodes=nodes, out_nodes=out_nodes)
    dataloader = DataLoader(reader, **{**cfg.data.loader_params, "shuffle": False}, collate_fn=reader.collate_fn)

    # median for ensemble edges in async NGC
    vote_fn = aggregation_fn_from_cfg(cfg)
    edge_model_fn = partial(get_model_type, str_model_type=cfg.model.type, model_args=cfg.model.parameters)
    graph = graph_cfg.build_model(nodes, edge_model_fn, vote_fn).to("cpu")
    print(LME(graph).summary)

    graph.eval()

    if args.weights_path.is_dir():
        graph.load_all_weights(args.weights_path)
    else:
        ckpt_data = tr.load(args.weights_path, map_location="cpu")
        assert ckpt_data["hyper_parameters"]["aggregation"] == cfg.graph.aggregation, ckpt_data["hyper_parameters"]
        TrainableNGC(graph, cfg.train).load_state_dict(ckpt_data["state_dict"])
    graph.to(device)
    dump_predictions(graph, dataloader, reader.out_nodes, args.output_path, args.dump_png)

if __name__ == "__main__":
    main()
