#!/usr/bin/env python3
"""no selection, no train dir, no training. Just edges and 'state' (aggregation/graph) evaluation."""
from argparse import ArgumentParser
from pathlib import Path
from natsort import natsorted
import numpy as np
import pandas as pd
from typing import Dict, Tuple, List
from numpy.typing import NDArray
from tqdm import tqdm
import os

def do_metric(y_data: NDArray["n,h,w,c"], gt_data: NDArray["n,h,w,c"]) -> NDArray["n"]:
    """Appies the metric_fn for the (y, gt) data. Returns the per-item metric score."""
    assert len(y_data) == len(gt_data), (y_data.shape, gt_data.shape)
    res = []
    # Apply the metric_for each aggregated item item individually
    for y, gt in zip(y_data, gt_data):
        res.append(l2(y, gt))
    res = np.array(res, dtype=np.float32)
    res[np.isnan(res)] = res[~np.isnan(res)].mean()
    assert res.shape == (len(y_data),), res.shape
    return res

# Read data and such

def read_all(paths: Dict[str, List[Path]]):
    res = {}
    for k, edge_paths in tqdm(paths.items(), desc="Read data"):
        edge_data = np.array([np.load(y)["arr_0"] for y in edge_paths], dtype=np.float32)
        edge_data[np.isnan(edge_data)] = 0
        res[k] = edge_data
    return res


def _natsort_iterdir(dir: Path) -> List[Path]:
    """iterdirs() a directory with expected predictions and returns the list of all files, natsorted"""
    return natsorted([x for x in dir.iterdir()], key=lambda x: x.name)

# def read_node_data_cache_fn(node_path: Path) -> str:
#     # Returns parent/node_name, so we can work with multiple iterations too, stored in different dump_dirs
#     return f"{node_path.parent.name}_{node_path.name}"

# @cache_fn(NpyFS, key_encode_fn=read_node_data_cache_fn)
def read_node_data(node_path: Path) -> Tuple[Dict[str, np.ndarray], np.ndarray]:
    """Traverseses the dump_dir structure of a node, reads edges predictions and gt and returns them."""
    gt_path = node_path / "gt/state/npy"
    gt_data_paths = _natsort_iterdir(gt_path)
    gt_data: np.ndarray = read_all({"GT": gt_data_paths})["GT"]

    all_timestamps = [x for x in node_path.iterdir() if x.name != "gt"]
    all_edges = {}
    for timestamp_dir in all_timestamps:
        t_edges = [edge_dir / "npy" for edge_dir in timestamp_dir.iterdir()]
        for t_edge in t_edges:
            # state = aggregations. We use 'state_0', 'state_1' for states. We use only edge name for non-states.
            name = f"state_{timestamp_dir.name}" if t_edge.parent.name == "state" else t_edge.parent.name
            if name in all_edges:
                print(f"t={timestamp_dir}. Edge '{t_edge.parent.name}'. Already read before, will overwrite.")
            all_edges[name] = _natsort_iterdir(t_edge)
    assert len(all_edges) > 0, f"'{node_path}' has 0 usable edges"
    y_data: Dict[str, np.ndarray] = read_all(all_edges)
    for k, v in y_data.items():
        assert v.shape == gt_data.shape, f"edge {k}. {v.shape} vs {gt_data.shape}"
    return y_data, gt_data


# Metrics stuff below

def do_edge_metrics(y_data, gt_data) -> pd.Series:
    res = {}
    for key in tqdm(y_data.keys(), desc="all metrics"):
        ys = y_data[key]
        out = do_metric(ys, gt_data)
        res[key] = {"l2": out.mean(), "l2_std": out.std(), "l2_each": tuple(round(x, 5) for x in out)}
    res = pd.DataFrame(res).T.sort_values("l2")
    res.index.name = "edges"
    res["l2 relative"] = rel(res["l2"].min(), res["l2"])
    return res


# simple metrics
def l2(y: NDArray["h,w,c"], gt: NDArray["h,w,c"]) -> float:
    assert len(y.shape) == 3 and len(gt.shape) == 3
    y = y.flatten().astype(float)
    gt = gt.flatten().astype(float)
    mask = gt != 0
    y = y[mask]
    gt = gt[mask]
    return ((y - gt) ** 2).mean()


def rel(baseline: NDArray["n"], x: NDArray["n"]) -> NDArray["n"]:
    return ((baseline / x - 1) * 100).astype(float)

def get_output_nodes(base_dir: Path):
    """Infering output nodes from base dir based on whethere there's only GT or there's predictions, too"""
    dump_dirs = tuple(x for x in Path(base_dir).iterdir() if x.is_dir())
    out_nodes = []
    for dump_dir in dump_dirs:
        if len(list(dump_dir.iterdir())) == 1:
            continue
        if dump_dir.name == "logs":
            continue
        if not (dump_dir / "gt/state/npy").exists():
            print(f"Potential out node: '{dump_dir.name}' has no GT data. Skipping.")
            continue
        out_nodes.append(dump_dir.name)
    return sorted(out_nodes)

def get_args():
    parser = ArgumentParser()
    parser.add_argument("dump_dir", type=lambda p: Path(p).absolute())
    args = parser.parse_args()
    assert Path(args.dump_dir).exists(), f"Dump dir '{args.dump_dir}' doesn't exist"
    return args

def main():
    args = get_args()
    out_nodes = get_output_nodes(args.dump_dir)
    assert len(out_nodes) > 0, f"No out nodes found at '{args.dump_dir}'"
    os.chdir(args.dump_dir)
    print(f"Dump dir: '{args.dump_dir}'.\nOut nodes: {out_nodes}")

    for node in (pbar := tqdm(out_nodes)):
        pbar.set_description(node)
        y_test, gt_test = read_node_data(args.dump_dir / node)
        print(f"Node: '{node}'. Test set: {gt_test.shape}. N voters: {len(y_test)}")

        # Get the results of all edges first on test set
        res_edges_out_file = Path(f"{args.dump_dir}/{node}_results_edges_test.csv")
        print(f"{res_edges_out_file}. Exists: {res_edges_out_file.exists()}")
        if not res_edges_out_file.exists():
            res_edges = do_edge_metrics(y_test, gt_test)
            res_edges.to_csv(res_edges_out_file, float_format="%.5f")
        else:
            res_edges = pd.read_csv(res_edges_out_file, index_col=0)

if __name__ == "__main__":
    main()
