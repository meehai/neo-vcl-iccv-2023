#!/usr/bin/env python3
from torch import nn
from torch.nn import functional as F
from lightning_module_enhanced import LME
import numpy as np
import torch as tr
from typing import List
from pathlib import Path
from torch.utils.data import DataLoader, Dataset
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import TensorBoardLogger
from torch import optim
from lightning_module_enhanced.callbacks import PlotMetrics
import pandas as pd

class ModelConv3D_V1(nn.Module):
    """
    540x1080 -> conv3ds -> B,V,W,H,D
    out = B,V,W,H,D (network directly outputs a prediction)
    """
    def __init__(self, n_votes: int, hidden_shapes: List[int]):
        super().__init__()
        assert len(hidden_shapes) >= 2

        shapes = [n_votes, *hidden_shapes]
        self.conv3ds = nn.Sequential()
        for i in range(1, len(shapes)):
            self.conv3ds.append(nn.Conv3d(in_channels=shapes[i - 1], out_channels=shapes[i], kernel_size=3, padding=1))
            self.conv3ds.append(nn.ReLU())
        self.conv3d_out = nn.Conv3d(in_channels=shapes[-1], out_channels=1, kernel_size=3, padding=1)

    def forward(self, x):
        y = self.conv3ds(x)
        y_out = self.conv3d_out(y)
        return y_out[:, 0]

class ModelConv3D_V2(nn.Module):
    """
    540x1080 -> 224x448 -> conv3ds -> B,V
    out = B,V,W,H,D x B,V-> B,V,W,H,D (1 weight for each image)
    """
    def __init__(self, n_votes: int, hidden_shapes: List[int]):
        super().__init__()
        assert len(hidden_shapes) >= 2

        shapes = [n_votes, *hidden_shapes, 1]
        self.conv3ds = nn.Sequential()
        for i in range(1, len(shapes)):
            self.conv3ds.append(nn.Conv3d(in_channels=shapes[i - 1], out_channels=shapes[i], kernel_size=3, padding=1))
            self.conv3ds.append(nn.ReLU())
        self.fc_out = nn.Linear(in_features=224 * 448, out_features=n_votes)

    def forward_weights(self, x: tr.Tensor) -> tr.Tensor:
        mb, voters, h, w, d = x.shape
        x = x.permute(0, 1, 4, 2, 3)
        x_reshape = F.interpolate(x.reshape(mb * voters, d, h, w), (224, 448)).reshape(mb, voters, d, 224, 448)
        x_reshape = x_reshape.permute(0, 1, 3, 4, 2)
        y = self.conv3ds(x_reshape)
        y_flat = y.reshape(mb, -1)
        y_weights = self.fc_out(y_flat)
        y_weights /= y_weights.sum(dim=1)[:, None]
        return y_weights

    def forward(self, x: tr.Tensor):
        y_weights = self.forward_weights(x)
        y_out = tr.einsum("bvwhd, bv -> bwhd", x, y_weights)
        return y_out

class ModelConv3D_V3(nn.Module):
    """
    540x1080 -> conv3ds -> B,V,W,H,D
    out = B,V,W,H,D x B,V,W,H,D -> B,V,W,H,D (1 weight for each pixel])
    """
    def __init__(self, n_votes: int, hidden_shapes: List[int]):
        super().__init__()
        assert len(hidden_shapes) >= 2
        self.n_votes = n_votes

        shapes = [n_votes, *hidden_shapes]
        self.conv3ds = nn.Sequential()
        for i in range(1, len(shapes)):
            self.conv3ds.append(nn.Conv3d(in_channels=shapes[i - 1], out_channels=shapes[i], kernel_size=3, padding=1))
            self.conv3ds.append(nn.ReLU())
        self.conv3d_out = nn.Conv3d(in_channels=shapes[-1], out_channels=n_votes, kernel_size=3, padding=1)

    def forward(self, x):
        y = self.conv3ds(x)
        y_mask = self.conv3d_out(y)
        y_out = tr.einsum("bvwhd, bvwhd -> bwhd", x, y_mask)
        return y_out

class Reader(Dataset):
    def __init__(self, y_data: np.ndarray, gt_data: np.ndarray):
        self.y_data = y_data.astype(np.float32)
        self.gt_data = gt_data.astype(np.float32)
        assert len(self.y_data) == len(self.gt_data)

    def __len__(self):
        return len(self.y_data)

    def __getitem__(self, ix):
        return {"data": self.y_data[ix], "labels": self.gt_data[ix]}

def loss_fn(y: tr.Tensor, gt: tr.Tensor) -> tr.Tensor:
    y = y[gt != 0]
    gt = gt[gt != 0]
    return (y - gt).pow(2).mean()

def train(model: LME, y_train: np.ndarray, gt_train: np.ndarray, y_test: np.ndarray, gt_test: np.ndarray,
          name: str, version: str) -> LME:
    model.optimizer = optim.Adam(model.parameters(), lr=0.01)
    model.scheduler_dict = {"scheduler": optim.lr_scheduler.ReduceLROnPlateau(model.optimizer, mode="min",
                                                                              factor=0.9, patience=30),
                            "monitor": "val_loss"}
    model.criterion_fn = loss_fn
    train_loader = DataLoader(Reader(y_train.transpose(1, 0, 2, 3, 4), gt_train), batch_size=5)
    test_loader = DataLoader(Reader(y_test.transpose(1, 0, 2, 3, 4), gt_test), batch_size=5)
    logger = TensorBoardLogger(save_dir="logs/", name=name, version=version)
    accelerator = "gpu" if tr.cuda.is_available() else "cpu"
    trainer = Trainer(max_epochs=100, accelerator=accelerator, logger=logger, callbacks=[PlotMetrics()])
    print(model.summary)
    trainer.fit(model, train_loader, test_loader)
    model.load_state_from_path(trainer.checkpoint_callback.best_model_path)
    return model

def nn_general(model: LME, y_train: np.ndarray, gt_train: np.ndarray, y_test: np.ndarray, gt_test: np.ndarray,
               node_name: str, variant: str, res_edges: pd.Series) -> np.ndarray:
    assert variant is not None
    version = f"{variant}/top_{len(y_train)}"
    weights_path = Path(f"logs/{node_name}/{version}/checkpoints/model_best.ckpt")
    if not weights_path.exists():
        model.save_hyperparameters({"edges": res_edges.index.tolist()})
        model = train(model, y_train, gt_train, y_test, gt_test, node_name, version)
    else:
        model.load_state_from_path(weights_path)

    assert "edges" in model.hparams
    y = model.np_forward(y_test.transpose(1, 0, 2, 3, 4).astype(np.float32)).numpy()
    return y


def nn_small_v1(y_train: np.ndarray, gt_train: np.ndarray, y_test: np.ndarray, gt_test: np.ndarray,
                node_name: str, variant: str, res_edges: pd.Series) -> np.ndarray:
    model = LME(ModelConv3D_V1(y_train.shape[0], [10, 5]))
    return nn_general(model, y_train, gt_train, y_test, gt_test, node_name, variant, res_edges)


def nn_small_v2(y_train: np.ndarray, gt_train: np.ndarray, y_test: np.ndarray, gt_test: np.ndarray,
                node_name: str, variant: str, res_edges: pd.Series) -> np.ndarray:
    model = LME(ModelConv3D_V2(y_train.shape[0], [10, 5]))
    return nn_general(model, y_train, gt_train, y_test, gt_test, node_name, variant, res_edges)


def nn_small_v3(y_train: np.ndarray, gt_train: np.ndarray, y_test: np.ndarray, gt_test: np.ndarray,
                node_name: str, variant: str, res_edges: pd.Series) -> np.ndarray:
    model = LME(ModelConv3D_V3(y_train.shape[0], [10, 5]))
    return nn_general(model, y_train, gt_train, y_test, gt_test, node_name, variant, res_edges)
