#!/usr/bin/env python3
"""
Downloads the nasa grayscale data. If --download is not set, it just outputs the csv of all the in links and out files.
"""
from pathlib import Path
from multiprocessing import Pool, cpu_count
from argparse import ArgumentParser
from datetime import timedelta
import requests
import shutil
from bs4 import BeautifulSoup
from tqdm import tqdm
import pandas as pd
import numpy as np

import matplotlib.pyplot as plt

def get_all_image_links_from_url(url: str) -> list[str]:
    """gets the image links from the NEO website"""
    req = requests.get(url, allow_redirects=True)
    bs_html = BeautifulSoup(req.content, features="html.parser")
    all_a = bs_html.find_all("a")
    all_a_png = [str(x) for x in all_a if str(x).find("PNG") != -1]
    all_png = [a_png.split("\">")[1][0:-4] for a_png in all_a_png]
    return all_png

def get_urls(which: str) -> list[list[str]]:
    if which == "1month":
        urls = [
            ["AOD", "MODAL2_M_AER_OD"],
            ["CHLORA", "MY1DMM_CHLORA"],
            ["CLD_FR", "MYDAL2_M_CLD_FR"],
            ["CLD_RD", "MODAL2_M_CLD_RD"],
            ["CLD_WP", "MYDAL2_M_CLD_WP"],
            ["COT", "MODAL2_M_CLD_OT"],
            ["FIRE", "MOD14A1_M_FIRE"],
            ["LAI", "MOD15A2_M_LAI"],
            ["LSTD_AN", "MOD_LSTAD_M"],
            ["LSTD", "MOD_LSTD_M"],
            ["LSTN_AN", "MOD_LSTAN_M"],
            ["LSTN", "MOD_LSTN_M"],
            ["NO2", "AURA_NO2_M"],
            ["OZONE", "AURA_OZONE_M"],
            ["SNOWC", "MOD10C1_M_SNOW"],
            ["SST", "MYD28M"],
            ["WV", "MODAL2_M_SKY_WV"],
            ["vegetation", "MOD_NDVI_M"],
            ["CarbonMonoxide", "MOP_CO_M"]
        ]
    else:
        # rip co_m
        urls = [
            # MCD?
            ["BS_ALBEDO", "MCD43C3_E_BSA"],
            # MODAL 1
            ["NDVI_16", "MOD_NDVI_16"],
            ["LSTD_AN", "MOD_LSTAD_E"],
            ["LSTD", "MOD_LSTD_E"],
            ["LSTN_AN", "MOD_LSTAN_E"],
            ["LSTN", "MOD_LSTN_E"],
            ["FIRE", "MOD14A1_E_FIRE"],
            ["LAI", "MOD15A2_E_LAI"],
            ["SNOWC", "MOD10C1_E_SNOW"],
            # MYDAL 1
            ["CHLORA", "MY1DMW_CHLORA"],
            ["SST", "MYD28W"],
            # MODAL 2 -- there are other files in MYDAL2_...
            ["AOD", "MODAL2_E_AER_OD"],
            ["WV", "MODAL2_E_SKY_WV"],
            ["COT", "MODAL2_E_CLD_OT"],
            ["CLD_FR", "MODAL2_E_CLD_FR"],
            ["CLD_RD", "MODAL2_E_CLD_RD"],
            ["CLD_WP", "MODAL2_E_CLD_WP"],
            # AURA
            ["NO2", "AURA_NO2_E"],
            ["OZONE", "AURA_OZONE_E"],
            # CERES
            ["INSOL", "CERES_INSOL_E"],
            ["NETFLUX", "CERES_NETFLUX_E"],
            ["LWFLUX", "CERES_LWFLUX_E"],
            ["SWFLUX", "CERES_SWFLUX_E"],
        ]
    return urls

def plot_timeline(url_to_out_file: pd.DataFrame, out_png_path: Path):
    """plots the timeline of the data based on the urls only w/o downloading"""
    res = {}
    for node in url_to_out_file.node.unique():
        res[node] = pd.to_datetime(url_to_out_file[url_to_out_file.node == node].particle).tolist()
        res[node] = [res[node][0], res[node][-1]]

    def extract_years(timestamps):
        years = {}
        for key, values in timestamps.items():
            years[key] = [value.year for value in values]
        return years

    min_date = min([x[0] for x in res.values()])
    max_date = max([x[1] for x in res.values()])
    plt.figure(figsize=(12, 5))
    for i, (key, (y_start, y_end)) in enumerate(res.items()):
        x: np.ndarray = np.arange(y_start, y_end, timedelta(days=30))
        y = [i] * len(x)
        plt.plot(x, y, 'o-')

    plt.gca().set_yticklabels(list(res.keys()))
    plt.gca().set_yticks(range(len(res)))
    plt.gca().set_xticks(np.arange(min_date, max_date, timedelta(days=365)))
    plt.gca().set_xticklabels([str(pd.to_datetime(x).year)[-2:]
                               for x in np.arange(min_date, max_date, timedelta(days=365))])
    plt.savefig(out_png_path)
    print(f"Saved timeline figure to '{out_png_path}'")

def do_one(x):
    in_url: str = x[0]
    out_file = Path(x[1])
    out_file.parent.mkdir(exist_ok=True, parents=True)
    if out_file.exists():
        return
    req = requests.get(in_url, allow_redirects=True, stream=True)
    with open(out_file, "wb") as fp:
        shutil.copyfileobj(req.raw, fp)
    del req

def get_url_to_particle_csv(urls: list[tuple[str, str]]) -> pd.DataFrame:
    """
    creates a csv, for each link of the format link, node_type, week or month
    Example:
    https://neo.gsfc.nasa.gov/archive/gs/MOD_LSTN_M/MOD_LSTN_M_2010-05.PNG,LSTN,2010-05
    """
    base_url = "https://neo.gsfc.nasa.gov/archive/gs"
    url_to_particle = []
    for name, url_name in urls:
        url = f"{base_url}/{url_name}"
        all_png = get_all_image_links_from_url(url)
        assert len(all_png) > 0, name
        print(f"Node '{name}' from url '{url}'. Got {len(all_png)} image urls.")
        for png_name in all_png:
            # AURA_NO2_E_2005-01-17.PNG -> 2005-01-17
            particle = png_name.split("_")[-1].split(".PNG")[0]
            image_link = f"{url}/{png_name}"
            url_to_particle.append([image_link, name, particle])
    url_to_out_file = pd.DataFrame(url_to_particle, columns=["link", "node", "particle"])
    return url_to_out_file

def get_args():
    parser = ArgumentParser()
    parser.add_argument("which", help="Valid options: 1month or 1week")
    parser.add_argument("--plot_timeline", action="store_true")
    parser.add_argument("--n_cores", type=int, default=-1)
    parser.add_argument("--download", action="store_true")
    parser.add_argument("--out_dir", "-o", type=Path)
    args = parser.parse_args()
    assert args.which in ("1month", "1week"), args.which
    if args.download:
        assert args.out_dir is not None, "Use --out_dir/-o to specify where to store the data"
    return args

def main():
    args = get_args()
    urls = get_urls(args.which)

    url_to_particle_file = Path(__file__).absolute().parent / f"url_to_particle_{args.which}.csv"
    if not url_to_particle_file.exists():
        url_to_out_file = get_url_to_particle_csv(urls)
        url_to_out_file.to_csv(url_to_particle_file)
        print(f"Stored '{url_to_particle_file}'")
    else:
        url_to_out_file = pd.read_csv(url_to_particle_file, index_col=0)
        print(f"Loaded '{url_to_particle_file}'")

    if args.plot_timeline:
        plot_timeline(url_to_out_file, Path(f"timeline_{args.which}.png"))

    if args.download:
        in_files = url_to_out_file.link
        out_files = (args.out_dir) / url_to_out_file.apply(lambda x: f"{x.node}/{x.particle}.png", axis=1)
        data = list(zip(in_files, out_files))
        map_fn = map if args.n_cores == 0 else (Pool(cpu_count() if args.n_cores == -1 else args.n_cores).imap)
        list(map_fn(do_one, tqdm(data)))

if __name__ == "__main__":
    main()
