#!/usr/bin/env python3
from pathlib import Path
import os
from natsort import natsorted
from argparse import ArgumentParser

def find_intersection(dirs: list[list[str]]) -> list[str]:
    """Given a list of lists, find the intersection of those lists"""
    assert len(dirs) >= 1
    res = set(dirs[0])
    for _dir in dirs[1: ]:
        res = res.intersection(_dir)
    return natsorted(list(res))

def write(files: list[str], nodes: list[str], in_dir: Path, out_dir: Path):
    for file in files:
        for node in nodes:
            (out_dir / node).mkdir(exist_ok=True, parents=True)
            in_file = in_dir / node / file
            out_file = out_dir / node / file
            if in_file.exists():
                os.symlink(in_file, out_file)

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--dataset_path", required=True)
    parser.add_argument("--in_nodes", nargs="+", required=True)
    parser.add_argument("--out_nodes", nargs="+", required=True)
    args = parser.parse_args()
    args.dataset_path = Path(args.dataset_path).absolute()
    assert len(args.in_nodes) > 0
    assert len(args.out_nodes) > 0
    return args

def main():
    args = get_args()
    in_nodes = args.in_nodes
    out_nodes = args.out_nodes
    print(f"In path: '{args.dataset_path}'")
    print(f"In nodes: {in_nodes}")
    print(f"Out nodes: {out_nodes}")

    full_path = Path(args.dataset_path).absolute()
    train_set_path = Path(args.dataset_path.parent / "train_set/").absolute()
    test_set_path = Path(args.dataset_path.parent / "test_set/").absolute()
    semisup_path = Path(args.dataset_path.parent / "semisupervised_set/").absolute()

    full_all_nodes = find_intersection([[x.name for x in (full_path / node).iterdir()]
                                        for node in [*in_nodes, *out_nodes]])
    full_in_nodes = find_intersection([[x.name for x in (full_path / node).iterdir()] for node in in_nodes])

    n_train = int(0.8 * len(full_all_nodes))
    train_set = full_all_nodes[0: n_train]
    test_set = full_all_nodes[n_train:]
    semisup_set = natsorted(list(set(full_in_nodes).difference(train_set)))
    semisup_set_no_test = natsorted(set(semisup_set).difference(test_set))

    print(f"Train set ({len(train_set)}): {train_set[0][0:-4]} -> {train_set[-1][0:-4]}")
    print(f"Test set ({len(test_set)}): {test_set[0][0:-4]} -> {test_set[-1][0:-4]}")
    print(f"Semisup set ({len(semisup_set)}): {semisup_set[0][0:-4]} -> {semisup_set[-1][0:-4]}")

    write(train_set, [*in_nodes, *out_nodes], in_dir=full_path, out_dir=train_set_path)
    write(test_set, [*in_nodes, *out_nodes], in_dir=full_path, out_dir=test_set_path)
    # semisup is made of two parts: full in nodes and partial gt nodes
    write(semisup_set_no_test, [*in_nodes, *out_nodes], in_dir=full_path, out_dir=semisup_path)
    write(test_set, in_nodes, in_dir=full_path, out_dir=semisup_path)
    print("Done!")

if __name__ == "__main__":
    main()
