#!/usr/bin/env python3
"""
Script to convert the raw png (grayscale) images from NEO dataset to numpy format (float32 [0:1])
Usage: python convert_png_to_npy.py /path/to/raw_dir /path/to/args.output_path
"""

from media_processing_lib.image import image_read, image_resize
from argparse import ArgumentParser
from pathlib import Path
from tqdm import tqdm, trange
import numpy as np
from functools import partial
from multiprocessing import Pool, cpu_count

def do_one(representation, base_dir: Path, out_dir: Path, resolution: tuple[int, int]):
    assert len(resolution) == 2
    aod_path: Path = base_dir / representation
    in_images_paths = [x for x in aod_path.iterdir() if x.suffix in (".PNG", ".png")]
    # in_images_paths = in_images_paths[0:20]
    images = [image_read(image) for image in tqdm(in_images_paths, desc="read images")]
    images = [image_resize(image, height=resolution[0], width=resolution[1]) for image in images]
    images = [image[..., 0:1] for image in images]
    # images: [0 : 255] as uint8
    float_images = np.float16(images)
    # convert white pixels to nans
    float_images[float_images == 255] = np.nan
    float_images[~np.isnan(float_images)] /= 255

    (out_dir / representation).mkdir(parents=True, exist_ok=True)
    for i in trange(len(float_images)):
        npz_name = in_images_paths[i].name.split(".")[0].split("_")[-1] # MODAL2_M_AER_OD_2001-04.PNG => 2001-04
        np.savez(out_dir / representation / npz_name, float_images[i])

def get_args():
    parser = ArgumentParser()
    parser.add_argument("raw_dir")
    parser.add_argument("--output_path", "-o", required=True)
    parser.add_argument("--representations", nargs="+")
    parser.add_argument("--resolution", nargs="+", type=int, default=[540, 1080])
    parser.add_argument("--n_cores", type=int, default=-1)
    args = parser.parse_args()
    args.raw_dir = Path(args.raw_dir).absolute()
    args.args.output_path = Path(args.args.output_path).absolute()
    if args.representations is None:
        args.representations = [x.name for x in args.raw_dir.iterdir() if x.is_dir()]
    assert len(args.representations) > 0, "No representations found"
    assert len(args.resolution) == 2, f"Expected height and width, got {args.resolution}"
    return args

def main():
    args = get_args()
    print(f"In path: '{args.raw_dir}'")
    print(f"Out path: '{args.args.output_path}'")
    print(f"Representations: {args.representations}")

    f = partial(do_one, base_dir=args.raw_dir, out_dir=args.args.output_path, resolution=args.resolution)
    map_fn = map if args.n_cores == 0 else (Pool(cpu_count() if args.n_cores == -1 else args.n_cores).imap)
    list(map_fn(f, args.representations))


if __name__ == "__main__":
    main()
